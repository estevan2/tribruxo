import "./style.css"

export default function OpeningMessage() {

    return (
        <div className="content__message">
            <h1>Torneio</h1>
            <p>Clique no Botão para encontrar os feiticeiros!</p>
        </div>
    )
}