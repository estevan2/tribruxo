import CardWitches from "../CardWitches"
import "./style.css"
import { useState } from "react"

export default function Witches({ witches }) {

    const randomWitch = () => {
        const randomWitch1 = witches[Math.floor(Math.random() * (witches.length -1))]
        const filtered = witches.filter( item => item.house !== randomWitch1.house)

        const randomWitch2 = filtered[Math.floor(Math.random() * (filtered.length -1))]
        const filtere2 = filtered.filter( item => item.house !== randomWitch2.house)

        const randomWitch3 = filtere2[Math.floor(Math.random() * (filtere2.length -1))]

        return [randomWitch1, randomWitch2, randomWitch3]
    }

    const triWitches = randomWitch()

    return (
        <div className="content__cards">
            <CardWitches witch={triWitches[0]} />
            <CardWitches witch={triWitches[1]} />
            <CardWitches witch={triWitches[2]} />
        </div>
    )
}