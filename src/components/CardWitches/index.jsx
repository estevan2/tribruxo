import './style.css'

export default function CardWitches({ witch }) {

    return (
        <div className={`card ${witch.house}`}>
            <img src={witch.image} alt="" />
            <p className="name">{witch.name}</p>
            <p className={witch.house}>{witch.house}</p>
        </div>
    )
}