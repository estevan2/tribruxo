import './App.css';
import { useState, useEffect } from 'react'
import OpeningMessage from './components/OpeningMessage';
import Witches from './components/Witches';

function App() {

  const [sideButton, setSideButton] = useState(false)

  const [witches, setWitches] = useState([])

    useEffect(() => {
        fetch('https://hp-api.herokuapp.com/api/characters/students')
        .then(( item => item.json()))
        .then(( item => setWitches(item)))
    }, [])
  
  const turnButton = () => {
    setSideButton(!sideButton)
  }

  return (
    <main>
      <div className="content">
         {sideButton ? <Witches witches={witches} /> : <OpeningMessage />}
      </div>
      <button onClick={turnButton}>{sideButton ? "Tentar Novamente" : "Começar!"}</button>
    </main>
  );
}

export default App;
